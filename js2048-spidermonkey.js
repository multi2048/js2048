#!/usr/bin/env -S js78 -f

/*
SpiderMonkey doesn't seem to support raw access to keyboard input, so you'll
have to type commands like in a nice old text adventure.
*/

load(['./js2048.js']);

class SpiderMonkey2048 {
    constructor() {
        this.game = new Js2048();
        this.commands = {
            'left':  [ 'a', 'h', '4', 'left', 'lef', 'le' ],
            'right': [ 'd', 'l', '6', 'right', 'righ', 'rig', 'ri' ],
            'up':    [ 'w', 'k', '8', 'up', 'u' ],
            'down':  [ 's', 'j', '2', 'down', 'dow', 'do' ],
            'quit':  [ 'q', 'x', '.', 'quit', 'exit', 'qui', 'qu', 'exi', 'ex' ]
        }
    }

    writeat( x, y, text) {
        print("\u001b["+y+";"+x+"H"+text);
    }

    printcommand(item) {
    }

    paintboard() {
        const borders = [ '+-------+-------+-------+-------+',
                          '|       |       |       |       |' ];
        //Clear the screen for painting
        print("\u001b[2J")
        //Draw unchanging stuff
        this.writeat(3,2,'Score:');
        this.writeat(3,3,'Turns:');
        for( let i = 0; i < 9; i++ ) {
            this.writeat(3,4+i, borders[i%2])
        }
        this.writeat(3,14, "Available commands:")

        var c = 0;
        var clen = 0;
        for( let cmd in this.commands ) {
            if( cmd.length > clen ) {
                clen = cmd.length;
            }
        }
        for( let cmd in this.commands ) {
            var tmp = this.commands[cmd].slice(0,4);
            var pretty = '';
            tmp.forEach(function(it, ix){
                pretty += it.substr(0,1).toUpperCase()+it.substr(1)+((ix < 3) ? ', ' : '');
            });
            this.writeat(5, 15+c, cmd.toUpperCase()+":")
            this.writeat(5+clen+2,15+c, pretty)
            c++;
        }
        this.writeat( 3, 21, "Command:");
    }

    paintstate() {
        this.writeat( 10,2, this.game.score);
        this.writeat( 10,3, this.game.turns);
        for( let r = 0; r < 4; r++ ) {
            for( let c = 0; c < 4; c++ ) {
                this.writeat(4+8*c,5+2*r, '       ')
                this.writeat(5+8*c,5+2*r, this.game.board[r][c])
            }
        }
    }

    run() {
        this.paintboard();
        this.game.addnum();
        this.game.addnum();
        run = true;
        while(run) {
            this.paintstate();
            this.writeat( 1, 22 ,"\u001b[J");
            this.writeat( 1, 21 ,"");
            var cmd = readline();
            cmd = cmd.toLowerCase();
            if( this.commands['left'].indexOf(cmd) != -1 ) {
                this.game.pushlt();
            } else if( this.commands['right'].indexOf(cmd) != -1 ) {
                this.game.pushrt();
            } else if( this.commands['up'].indexOf(cmd) != -1 ) {
                this.game.pushup();
            } else if( this.commands['down'].indexOf(cmd) != -1 ) {
                this.game.pushdn();
            } else if( this.commands['quit'].indexOf(cmd) != -1 ) {
                run = false;
            }
        }
    }
}

function main() {
    var game = new SpiderMonkey2048();
    game.run();
}
main();
