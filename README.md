# js2048
Implementation of 2048 in Javascript.

# js2048.js
A library for game logic. Used by each of the interfaces.

# js2048-html5.{css,html,js}
Web interface. OIpen the HTML file in a relatively modern web browser and it
**should** just work.

# js2048-node.js
Interface to run the game under Node.js

# js2048-spidermonkey.js
Interface for Mozilla SpiderMonkey (js78 specifically, **may** work under other
versions).
