#!/usr/bin/env node

const Js2048 = require('./js2048.js');

class Node2048 {

    constructor() {
        this.game = new Js2048();
    }

    writeat( x, y, text) {
        console.log("\u001b["+y+";"+x+"H"+text);
    }

    paintboard() {
        var borders = [ '+-------+-------+-------+-------+', '|       |       |       |       |' ];
        //Clear the screen for painting
        console.log("\u001b[2J")
        //Draw unchanging stuff
        this.writeat(3,2,'Score:');
        this.writeat(3,3,'Turns:');
        for (let i = 0; i < 9; i++ ) {
            this.writeat(3,4+i, borders[i%2])
        }
        this.writeat(3,14, "Controls: WASD, HJKL, 4826, Arrows")
        this.writeat(3,15, "Quit: ^C, ^X. ^Q, Q, X, .")
    }

    paintstate() {
        this.writeat(10,2,this.game.score);
        this.writeat(10,3,this.game.turns);
        const clr = "     ";
        for (var r in this.game.board) {
            for(var c in this.game.board[r]) {
                this.writeat(5+8*c,5+2*r,clr)
                if(this.game.board[r][c]) {
                    this.writeat(5+8*c,5+2*r,this.game.board[r][c])
                }
            }
        }
        this.writeat(1,16,"");
    }

    run() {
        var stdin = process.stdin;
        var g = this;
        stdin.setRawMode(true);
        stdin.resume();
        stdin.setEncoding('utf8');
        this.paintboard();
        this.game.addnum();
        this.game.addnum();
        this.paintstate();
        stdin.on('data', function(key) {
            if ([ '\u0003', '\u0017', '\u0024', '\u0026', '\u0027', 'q', 'Q', 'x', 'X', '.' ].indexOf(key) !== -1 ) {
                process.exit();
            } else if ([ '\u001b\u005b\u0044', 'a', 'A', 'h', 'H', '4' ].indexOf(key) !== -1) {
                g.game.pushlt();
            } else if ([ '\u001b\u005b\u0043', 'd', 'D', 'l', 'L', '6' ].indexOf(key) !== -1) {
                g.game.pushrt();
            } else if ([ '\u001b\u005b\u0041', 'w', 'W', 'k', 'K', '8' ].indexOf(key) !== -1) {
                g.game.pushup();
            } else if ([ '\u001b\u005b\u0042', 's', 'S', 'j', 'J', '2' ].indexOf(key) !== -1) {
                g.game.pushdn();
            }
            g.paintstate();
        });
    }
}

function main() {
    game = new Node2048();
    game.run();
};
main();
