class Js2048 {
    constructor() {
        this.board = [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]];
        this.score = 0;
        this.turns = 0;
    }

    addnum() {
        var t = []
        let r, c;
        //Figure out which locations are zeroes
        for (r in this.board) {
            for (c in this.board[r]) {
                if (this.board[r][c] === 0) {
                    t.push([r,c]);
                }
            }
        }
        //Pick a random zero-location
        t = t[Math.floor(t.length * Math.random())];
        //Decide between 2 or 4 (1/10 chance for 4).
        const v = Math.floor(10 * Math.random())===0?4:2;
        //Set that location to that value
        this.board[t[0]][t[1]] = v;
    }

    mirror() {
        for (var r in this.board) {
            this.board[r].reverse();
        }
    }

    invert() {
        var t = [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]];
        let r, c;
        for (r in this.board) {
            for (c in this.board[r]) {
                t[c][r] = this.board[r][c];
            }
        }
        this.board = t;
    }

    pushlt() {
        var t = [];
        let merged, tr, tc, equal, r, c;
        for (r in this.board) {
            t.push([]);
            tr = t.length-1;
            merged = false;
            for (c in this.board[r]) {
                tc = t[tr].length-1;
                if (this.board[r][c] > 0) {
                    if (!merged && (t[tr].length > 0) && (t[tr][tc] === this.board[r][c])) {
                        //The previous number is the same as this one, and it was not merged, so merge.
                        t[tr][tc] *= 2;
                        this.score += t[tr][tc];
                        merged = true;
                    } else {
                        t[tr].push(this.board[r][c]);
                        merged = false;
                    }
                }
            }
        }
        for (r in t) {
            while (t[r].length < 4) {
                t[r].push(0);
            }
        }
        equal = true;
        comparison:
        for (r in t) {
            for (c in t[r]) {
                if (t[r][c] !== this.board[r][c]) {
                    equal = false;
                    break comparison;
                }
            }
        }
        if (!equal) {
            this.turns += 1;
            this.board = t
            this.addnum();
        }
    }

    pushrt() {
        this.mirror();
        this.pushlt();
        this.mirror();
    }

    pushup() {
        this.invert();
        this.pushlt();
        this.invert();
    }

    pushdn() {
        this.invert();
        this.pushrt();
        this.invert();
    }
}

/*
var g = new Js2048();
g.pushlt();
*/

// For Node
if(typeof module != 'undefined') {
    module.exports = Js2048;
}
