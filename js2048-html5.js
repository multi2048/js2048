class Js2048Html5 {
    constructor() {
        this.game = new Js2048();
    }

    showstate() {
        var cell = 0;
        var elm, digits, num, r, c;
        for (r in this.game.board) {
            for (c in this.game.board[r]) {
                cell += 1;
                elm = document.querySelector(`#gameboard > div:nth-child(${cell})`);
                num = Math.min(this.game.board[r][c], 4096);
                digits = this.game.board[r][c].toString().length;
                elm.className = `d${digits} n${num}`;
                if (this.game.board[r][c] > 0) {
                    elm.textContent = this.game.board[r][c];
                } else {
                    elm.textContent = "";
                }
            }
        }
        document.querySelector("#score").textContent = this.game.score;
        document.querySelector("#turns").textContent = this.game.turns;
    }

    pushlt() {
        this.game.pushlt();
        this.showstate();
    }
    pushrt() {
        this.game.pushrt();
        this.showstate();
    }
    pushup() {
        this.game.pushup();
        this.showstate();
    }
    pushdn() {
        this.game.pushdn();
        this.showstate();
    }

    reset() {
        this.game = new Js2048();
        this.game.addnum();
        this.game.addnum();
        this.showstate();
    }

}

var game = new Js2048Html5();
window.onload = function() {
    game.game.addnum();
    game.game.addnum();
    game.showstate();
    document.addEventListener("keydown", function(ev) {
        if (ev.defaultPrevented) {
            return;
        }

        switch (ev.code) {
            case "ArrowLeft":
            case "KeyH":
            case "KeyA":
            case "Numpad4":
                game.pushlt();
                ev.preventDefault();
                break;
            case "ArrowRight":
            case "KeyL":
            case "KeyD":
            case "Numpad6":
                game.pushrt();
                ev.preventDefault();
                break;
            case "ArrowUp":
            case "KeyK":
            case "KeyW":
            case "Numpad8":
                game.pushup();
                ev.preventDefault();
                break;
            case "ArrowDown":
            case "KeyJ":
            case "KeyS":
            case "Numpad2":
                game.pushdn();
                ev.preventDefault();
                break;
            case "Escape":
            case "Period":
            case "KeyQ":
                game.reset();
                ev.preventDefault();
                break;
        }
    });
};
